import {get} from '@loopback/rest';
export class SortMonthsController {
  @get('/sortMonths')
  sortMonths(): any[]{
    let months = [
      {nombre: 'diciembre', numero: 12},
      {nombre: 'septiembre', numero: 9},
      {nombre: 'junio', numero: 6},
      {nombre: 'mayo', numero: 5},
      {nombre: 'noviembre', numero: 11},
      {nombre: 'marzo', numero: 3},
      {nombre: 'abril', numero: 4},
      {nombre: 'agosto', numero: 8},
      {nombre: 'enero', numero: 1},
      {nombre: 'febrero', numero: 2},
      {nombre: 'julio', numero: 7},
      {nombre: 'octubre', numero: 10},

    ]
    console.log(months.sort((prev, next) => {
      return prev.numero - next.numero;
    }))
    return months;
  }
}
